#include <iostream>
#include <windows.h>
using namespace std;

int main()
{
    int pilih, pilih1, pilih2, pilih3;
    while (1)
    {
        cout << "===========================================================" << endl
             << "                PROGRAM ANTRIAN RUMAH SAKIT                " << endl
             << "===========================================================" << endl
             << endl
             << "------------ MENU ------------" << endl
             << "1. Pasien Baru" << endl
             << "2. Update Data Pasien" << endl
             << "3. Lihat Antrian Pasien" << endl
             << "4. Pasien Keluar Dari Antrian" << endl
             << "5. Keluar Program" << endl
             << endl
             << "Pilih menu : ";
        cin >> pilih;

        switch (pilih)
        {
        case 1:
            cout << "===========================================================" << endl
                 << "                  MENAMBAHKAN PASIEN BARU                  " << endl
                 << "===========================================================" << endl
                 << endl
                 << "------------- LIST DOKTER -----------" << endl
                 << "1.  dr. Rahman          (Dokter Umum)" << endl
                 << "2.  dr. Arya            (Dokter Umum)" << endl
                 << "3.  dr. Fanny           (Dokter Umum)" << endl
                 << "4.  dr. Bima            (Dokter Anak)" << endl
                 << "5.  dr. Mannan          (Dokter Anak)" << endl
                 << "6.  dr. Tasya           (Dokter Spesialis THT)" << endl
                 << "7.  dr. Burhan          (Dokter Spesialis Penyakit Dalam)" << endl
                 << "8.  dr. Nusa            (Dokter Spesialis Penyakit Dalam)" << endl
                 << "9.  dr. Mawar           (Dokter Gigi)" << endl
                 << "10. dr. Boyke           (Dokter Spesialis Kulit & Kelamin)" << endl
                 << endl
                 << "Pilih Dokter : 1" << endl
                 << endl
                 << "=================== MASUKKAN DATA PASIEN ==================" << endl
                 << "Nama        : Yehezkiel Yeski Situmorang" << endl
                 << "Usia        : 20" << endl
                 << "Keluhan     : Badan panas sudah 3 hari" << endl
                 << endl
                 << "========== PASIEN BERHASIL MASUK KE DALAM ANTRIAN =========" << endl
                 << "Nama        : Yehezkiel Yeski Situmorang" << endl
                 << "Usia        : 20 tahun" << endl
                 << "Keluhan     : Badan panas sudah 3 hari" << endl
                 << "Dokter      : dr. Rahman" << endl
                 << "No. Antrian : 1" << endl;

            break;

        case 2:
            cout << "===========================================================" << endl
                 << "                     UPDATE DATA PASIEN                    " << endl
                 << "===========================================================" << endl
                 << endl
                 << "Masukkan nama pasien : Yehezkiel Yeski Situmorang" << endl
                 << "Mencari data pasien............." << endl
                 << endl
                 << "=================== DATA PASIEN DITEMUKAN ==================" << endl
                 << "Nama        : Yehezkiel Yeski Situmorang " << endl
                 << "Usia        : 20 tahun" << endl
                 << "Keluhan     : Badan panas sudah 3 hari" << endl
                 << "Dokter      : dr. Rahman" << endl
                 << "No. Antrian : 1" << endl
                 << endl
                 << "Apakah pasien ingin megubah dokter yang akan dikonsultasikan ? (y/t)" << endl
                 << "t" << endl
                 << endl
                 << "=============== MASUKKAN DATA PASIEN YANG BENAR =============" << endl
                 << "Nama        : Yehezkiel Yeski Hutabarat" << endl
                 << "Usia        : 21" << endl
                 << "Keluhan     : Badan panas dan menggigil sudah 3 hari" << endl
                 << endl
                 << "=============== DATA PASIEN BERHASIL DIPERBARUI =============" << endl
                 << "Nama        : Yehezkiel Yeski Hutabarat" << endl
                 << "Usia        : 21 tahun" << endl
                 << "Keluhan     : Badan panas dan menggigil sudah 3 hari" << endl
                 << "Dokter      : dr. Rahman" << endl
                 << "No. Antrian : 1" << endl;
            break;

        case 3:
            cout << "===========================================================" << endl
                 << "                       ANTRIAN PASIEN                      " << endl
                 << "===========================================================" << endl
                 << endl
                 << "-----------" << endl
                 << "dr. Rahman" << endl
                 << "-----------" << endl
                 << "No. Antrian : 1" << endl
                 << "Nama        : Yehezkiel Yeski Hutabarat" << endl
                 << "Usia        : 21 tahun" << endl
                 << "Keluhan     : Badan panas dan menggigil sudah 3 hari" << endl
                 << endl
                 << "No. Antrian : 3" << endl
                 << "Nama        : Bambang Nasution" << endl
                 << "Usia        : 40 tahun" << endl
                 << "Keluhan     : Sakit Kepala" << endl
                 << endl
                 << "-----------" << endl
                 << "dr. Tasya" << endl
                 << "-----------" << endl
                 << "No. Antrian : 2" << endl
                 << "Nama        : Rafa Dwi Pertiwi" << endl
                 << "Usia        : 18 tahun" << endl
                 << "Keluhan     : Telinga Berdengung" << endl
                 << endl
                 << "===========================================================" << endl
                 << "-------------- MENU --------------" << endl
                 << "1. Urutkan Berdasarkan Dokter" << endl
                 << "2. Urutkan Berdasarkan Abjad Pasien" << endl
                 << "3. Urutkan Berdasarkan No. Antrian" << endl
                 << "4. Kembali ke menu awal" << endl
                 << endl
                 << "Pilih menu : 2" << endl;

            cout << "===========================================================" << endl
                 << "                       ANTRIAN PASIEN                      " << endl
                 << "===========================================================" << endl
                 << endl

                 << "Nama        : Bambang Nasution" << endl
                 << "Usia        : 40 tahun" << endl
                 << "Keluhan     : Sakit Kepala" << endl
                 << "Dokter      : dr. Rahman"
                 << "No. Antrian : 3" << endl
                 << endl
                 << "Nama        : Rafa Dwi Pertiwi" << endl
                 << "Usia        : 18 tahun" << endl
                 << "Keluhan     : Telinga Berdengung" << endl
                 << "Dokter      : dr. Tasya" << endl
                 << "No. Antrian : 2" << endl
                 << endl
                 << "Nama        : Yehezkiel Yeski Hutabarat" << endl
                 << "Usia        : 21 tahun" << endl
                 << "Keluhan     : Badan panas dan menggigil sudah 3 hari" << endl
                 << "Dokter      : dr. Rahman" << endl
                 << "No. Antrian : 1" << endl
                 << "===========================================================" << endl
                 << endl
                 << "-------------- MENU --------------" << endl
                 << "1. Urutkan Berdasarkan Dokter" << endl
                 << "2. Urutkan Berdasarkan Abjad Pasien" << endl
                 << "3. Urutkan Berdasarkan No. Antrian" << endl
                 << "4. Kembali ke menu awal" << endl
                 << endl
                 << "Pilih menu : 3" << endl;

            cout << "===========================================================" << endl
                 << "                       ANTRIAN PASIEN                      " << endl
                 << "===========================================================" << endl
                 << endl
                 << "No. Antrian : 1" << endl
                 << "Nama        : Yehezkiel Yeski Hutabarat" << endl
                 << "Usia        : 21 tahun" << endl
                 << "Keluhan     : Badan panas dan menggigil sudah 3 hari" << endl
                 << "Dokter      : dr. Rahman" << endl
                 << endl
                 << "No. Antrian : 2" << endl
                 << "Nama        : Rafa Dwi Pertiwi" << endl
                 << "Usia        : 18 tahun" << endl
                 << "Keluhan     : Telinga Berdengung" << endl
                 << "Dokter      : dr. Tasya" << endl
                 << endl
                 << "No. Antrian : 3" << endl
                 << "Nama        : Bambang Nasution" << endl
                 << "Usia        : 40 tahun" << endl
                 << "Keluhan     : Sakit Kepala" << endl
                 << "Dokter      : dr. Rahman"<<endl
                 << "===========================================================" << endl
                 << endl
                 << "-------------- MENU --------------" << endl
                 << "1. Urutkan Berdasarkan Dokter" << endl
                 << "2. Urutkan Berdasarkan Abjad Pasien" << endl
                 << "3. Urutkan Berdasarkan No. Antrian" << endl
                 << "4. Kembali ke menu awal" << endl
                 << endl
                 << "Pilih menu : 4" << endl;
            break;

        case 4:
            cout << "===========================================================" << endl
                 << "              MENGELUARKAN PASIEN DARI ANTRIAN             " << endl
                 << "===========================================================" << endl
                 << "PILIH DOKTER YANG PASIENNYA SUDAH SELESAI DITANGANI" << endl
                 << "1. dr. Rahman" << endl
                 << "2. dr. Tasya" << endl
                 << endl
                 << "Pilih dokter : 1" << endl
                 << endl
                 << "No. Antrian : 1" << endl
                 << "Nama        : Yehezkiel Yeski Hutabarat" << endl
                 << "Usia        : 21 tahun" << endl
                 << "Keluhan     : Badan panas dan menggigil sudah 3 hari" << endl
                 << "=========== PASIEN BERHASIL KELUAR DARI ANTRIAN ===========" << endl;
            break;

        case 5:
            cout << "===========================================================" << endl
                 << "                     PROGRAM BERAKHIR                      " << endl
                 << "===========================================================" << endl;
            return 0;
        }
    }
}
